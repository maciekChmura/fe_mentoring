# Goal:

Implement routing system for Single Page Application using History API

Familiarize with fetch API and Promise

Practice
Develop single-page application.

## Part 1 - Single Page Application

Create blank web application consisting of index.html and index.js

Add three links “Home”, “Sign-In”, “Sign-up” on top of a page.

Under these links add following markup:

```
<main id="app">

 <!-- route content to be rendered here -->

</main>
```

In the script file define routes. Each route should have a relative url and HTML template.

```
const routes = [

{ url: '/', template: '<h1>Home</h1>' },

{ url: '/sign-in', template: '<h1>Sign-In</h1>' },

{ url: '/sign-up', template: '<h1>Sign-Up</h1>' }

];
```

Using History API implement a navigation by defined routes:

By default, ‘/’ route should be rendered inside <main> element

When you click any of the links on the top, URL should change in browser’s address bar and corresponding HTML should be rendered inside <main> element

Browser’s back/forward buttons should work

Notes:

You’ll need to handle “click” event of navigation links and prevent default behavior.

If you open web application from file system, you’ll likely to face a problem with navigation. This is due to browser’s security policies. To fix this serve files on a local webserver. For instance you may use node-static in command-line mode (see Command Line Interface in documentation).

If you have navigated to some route, e.g. to “/sign-in” and refresh the page - you’ll see server error. For this task such behavior is tolerable.

Part 2 - Asynchronous template loading

Create files home.html, sign-in.html, sign-up.html in src folder

Update routes by replacing template with templateUrl:

```
const routes = [

{ url: '/', templateUrl: 'home.html' },

{ url: '/sign-in', templateUrl: 'sign-in.html' },

{ url: '/sign-up', templateUrl: 'sign-up.html' }

];
```

Using fetch API, load template on route change and insert its content in the <main>.

Part 3

Integrate your solution into previously developed application (City Guide):

Header and Footer should be rendered regardless of current route.

Main content should be dependent on current route:

“/” - all markup you’ve previously created

“/sign-in” - for now only add simple heading there. Link “Sign In” in the header should navigate to this route.

As result of your work, provide a link to repository tag or branch (City Guide).

Materials
https://css-tricks.com/using-the-html5-history-api/

History API
https://developer.mozilla.org/en-US/docs/Web/API/History_API
History API Events
https://developer.mozilla.org/en-US/docs/Web/Events/popstate

Fetch API
https://davidwalsh.name/fetch

Promise
https://davidwalsh.name/promises
