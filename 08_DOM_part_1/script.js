const routes = [
  { url: "/", template: "<h1>Home</h1>" },

  { url: "/sign-in", template: "<h1>Sign-In</h1>" },

  { url: "/sign-up", template: "<h1>Sign-Up</h1>" }
];

let home = document.getElementById("home");
let singIn = document.getElementById("signin");
let singUp = document.getElementById("signup");
let main = document.getElementById("app");

home.addEventListener("click", e => followLink(e, 0));

singIn.addEventListener("click", e => followLink(e, 1));

singUp.addEventListener("click", e => followLink(e, 2));

function followLink(e, routeId) {
  e.preventDefault();
  history.pushState(null, null, routes[routeId].url);
  main.innerHTML = routes[routeId].template;
}
