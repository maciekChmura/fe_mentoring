let string = "2 1 + 5 *";

function evaluate(input) {
  let data = input.split(" ");
  let stack = [];
  let operand = /\w/;
  let operator = /\D/;
  let value = 0;
  for (let i = 0; i < data.length; i++) {
    const element = data[i];
    let operand1;
    let operand2;
    if (operator.test(data[i])) {
      if (data[i] === "+") {
        operand1 = stack.pop();
        operand2 = stack.pop();
        value = operand2 + operand1;
        stack.push(value);
      } else if (data[i] === "-") {
        operand1 = stack.pop();
        operand2 = stack.pop();
        value = operand2 - operand1;
        stack.push(value);
      } else if (data[i] === "*") {
        operand1 = stack.pop();
        operand2 = stack.pop();
        value = operand2 * operand1;
        stack.push(value);
      }
    } else if (operand.test(data[i])) {
      stack.push(Number(data[i]));
    }
  }
  if (!value) {
    throw "error, could not parse input";
  }
  return value;
}

console.log(evaluate(string));

module.exports = evaluate;

// calculator online:
//http://www.meta-calculator.com/learning-lab/reverse-polish-notation-calculator.php
