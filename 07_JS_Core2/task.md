Practice
Goal:

Develop tic-tac-toe game for two players.

Game should run in a browser.
UI could be as simple as plain text output.
Alternatively, you may implement a game as a set of DOM elements with CSS styling.

At startup, the game should print 3x3 field populated with dash symbols.
Current cursor position is indicated with square symbol.

Player X moves

─ ─ ─

─ ⬚ ─

─ ─ ─

Player may use arrow keys on a keyboard to navigate cursor within a field.

Current player name (X or O) is indicated above the field.

Players, in turn, use spacebar key on a keyboard to indicate their move, e.g.:

Player O moves

─ ─ ─

─ ⬚ X

─ ─ ─

When player indicates his move, there must be 300ms delay before “-” turns into “X” or “O”

Player can only make a move on empty cell (indicated with dash).

The game should analyze the status and once game is over, should display “Player X/O won” or “Draw”

When game is over, any key press should start it over.

Materials

Script Tag
http://javascript.crockford.com/script.html

Event Loop and Concurrency Model
https://developer.mozilla.org/en/docs/Web/JavaScript/EventLoop

Event Loop - more explanation, recommended!
http://altitudelabs.com/blog/what-is-the-javascript-event-loop/

Introduction to DOM
https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model/Introduction

Core Interfaces in the DOM
https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model/Introduction#Core_Interfaces_in_the_DOM

DOM Events
https://www.smashingmagazine.com/2013/11/an-introduction-to-dom-events/
