let assert = require("assert"); // node.js core module
let evaluate = require("../evaluate");

describe("Array", function() {
  describe("#indexOf()", function() {
    it("should return -1 when the value is not present", function() {
      assert.equal(-1, [1, 2, 3].indexOf(4)); // 4 is not present in this array so indexOf returns -1
    });
  });
});

describe("evaluate", function() {
  it("should evaluate string: 2 1 + 5 *  to  15", function() {
    assert.equal(15, evaluate("2 1 + 5 *"));
  });
});
