const evaluate = require("./evaluate");

it("jest works", () => {
  expect(1).toBe(1);
});

it("evaluate", () => {
  expect(evaluate("2 1 + 5 *")).toBe(15);
});
