let wd = (function() {
  //reference for future singleton
  let instance;

  //function to create new singleton
  function init() {
    let name;
    let routesArray = [];
    return {
      publicMethod: function() {
        console.log("I am public method");
      },
      createRoot: function(string) {
        name = string;
        init();
      },
      root: function() {
        return name;
      },
      routes: function(array) {
        routesArray.push(...array);
      },
      navigate: function(e, routeId) {
        e.preventDefault();

        fetch(`/src/${routesArray[routeId].templateUrl}`)
          .then(data => data.text())
          .then(data => {
            history.pushState(null, null, routesArray[routeId].url);
            app.innerHTML = data;
          });
      }
    };
  }

  //get the singleton instance
  //or create new if it doesn't
  return {
    getInstance: function() {
      if (!instance) {
        instance = init();
      }
      return instance;
    }
  };
})();
