Skip to end of metadata
Created by Artur Zahreba on May 10, 2017 Go to start of metadata
Goal:
Get familiar with OOP principles in JS

Prototypal inheritance, this

Practice
Goal:

In this module we’ll refactor previously created application.

We’ll separate it into 2 parts:

General-purpose framework

Application, build upon this framework

Note: In code samples below our brand-new framework is call “wd” - abbreviation of “Web Development”.

Description:

Part 1 - Set up

Update layout of the app to match:

```
index.html

wd.js - we’ll add framework code here
app.js - your app’s code

… all other files
```

Make sure, in index.html wd.js precedes app.js. (We want framework to load first).

Update `<main>` tag in index.html to match:

```
<main wd-root="app"></main>

<!-- wd-root is a custom attribute, our framework will use it to locate application root element. “app” is used as identifier and could be anything else. -->
```

Add following code to app.js

```
wd.createRoot('app')

.routes([

{ url: '...', templateUrl: '... },

// routes go here

]);
```

Part 2 - Framework

Now it’s time to implement a framework by the following requirements:

Framework should only expose 1 global variable - wd

Interface:

wd methods:

wd.createRoot(string) - creates new instance of the application (with specified name) and returns it for chaining. See example of usage in Part 1.

wd.root(string) - returns previously created instance of the application by specified name.

Instance methods (to be used as public):

.routes(routes[ ]) - defines routes available in the application. Used at application bootstrap.

.navigate(string) - navigates to route specified by url. Used when user clicks a link.

Note: you may want to add other methods or properties to be used as private.

Framework should automatically look for “wd-root” attribute on the DOM and bootstrap the application.

Note: theoretically there could be multiple application instances working on the same page. However routing make sense only for one. This is an assumption our framework has.

Materials
Introducing JavaScript objects
https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects

JavaScript object basics
https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/Basics

Constructors and object instances
https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/Object-oriented_JS#Constructors_and_object_instances

Object prototypes
https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/Object_prototypes

Inheritance in JavaScript
https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/Inheritance

Understand JavaScript’s “this” With Clarity, and Master It
http://javascriptissexy.com/understand-javascripts-this-with-clarity-and-master-it/

this
https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Operators/this

Private Members in JavaScript
http://www.crockford.com/javascript/private.html
