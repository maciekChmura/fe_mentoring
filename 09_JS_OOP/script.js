const routes = [
  { url: "/", templateUrl: "home.html" },

  { url: "/sign-in", templateUrl: "sign-in.html" },

  { url: "/sign-up", templateUrl: "sign-up.html" }
];

let home = document.getElementById("home");
let singIn = document.getElementById("signin");
let singUp = document.getElementById("signup");
let main = document.getElementById("app");

home.addEventListener("click", e => followLink(e, 0));

singIn.addEventListener("click", e => followLink(e, 1));

singUp.addEventListener("click", e => followLink(e, 2));

function followLink(e, routeId) {
  e.preventDefault();
  fetch(`/src/${routes[routeId].templateUrl}`)
    .then(data => data.text())
    .then(data => {
      history.pushState(null, null, routes[routeId].url);
      main.innerHTML = data;
    });
}
