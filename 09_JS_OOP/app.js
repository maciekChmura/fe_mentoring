let routes = [
  { url: "/", templateUrl: "home.html" },
  { url: "/sign-in", templateUrl: "sign-in.html" },
  { url: "/sign-up", templateUrl: "sign-up.html" }
];
let home = document.getElementById("home");
let singIn = document.getElementById("signin");
let singUp = document.getElementById("signup");
let main = document.getElementById("app");

home.addEventListener("click", e => frame.navigate(e, 0));

singIn.addEventListener("click", e => frame.navigate(e, 1));

singUp.addEventListener("click", e => frame.navigate(e, 2));

let app = document.querySelector("main[wd-root='app']");

let frame = wd.getInstance();
frame.routes(routes);
